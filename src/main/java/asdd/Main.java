package asdd;

import java.util.ResourceBundle;

import org.apache.commons.cli.*;

import asdd.models.Database;

public class Main {
	
	static String user = null;
	static String pass = null;
	static String host = null;
	static String database1 = null;
	static String database2 = null;
	static String type = null;
	static String driver;
	static String url;

	private static Options createOptions() {
		Options options = new Options();
		options.addOption("u", true, "Usuario");
		options.addOption("p", true, "Contrasenia");
		options.addOption("i", true, "Host");
		options.addRequiredOption("d1", "database1", true, "Nombre de la primer base de datos ");
		options.addRequiredOption("d2", "database2", true, "Nombre de la segunda base de datos ");

		OptionGroup group = new OptionGroup();
		group.addOption(new Option("mp", "Motor Postgresql"));
		group.addOption(new Option("mo", "Motor Oraclesql"));
		group.addOption(new Option("my", "Motor Mysql"));
		options.addOptionGroup(group);

		options.addOption("h", "Muestra este mensaje");
		return options;
	}

	public static void parsero(String[] args) {
		Options options = createOptions();
		
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
	    
	    try {
			cmd = parser.parse( options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			new HelpFormatter().printHelp("Mein", options);
			System.exit(0);
		}
	
	    if(cmd.hasOption("h")) { 
	    	new HelpFormatter().printHelp("Mein", options);
	    	System.exit(0);
	    }
	    user = cmd.getOptionValue("u");
	    pass = cmd.getOptionValue("p"); 
	    host = cmd.getOptionValue("i"); 
	    database1 = cmd.getOptionValue("d1");
	    database2 = cmd.getOptionValue("d2");
	    if (cmd.hasOption("mp")){
	    	type = "postgre";
	    	return;
	    }
	    if (cmd.hasOption("my")){
	    	type = "mysql";
	    	return;
	    }
	    if (cmd.hasOption("mpo")){
	    	type = "oracle";
	    	return;
	    }
	    
	    new HelpFormatter().printHelp("Mein", options);
    	System.exit(0);
	}
	private static void fill() {
		ResourceBundle rb = ResourceBundle.getBundle("database");
		if (user == null)
			user = rb.getString("database."+type+".username");
		if (pass == null)
			pass = rb.getString("database."+type+".password");
		if (host == null)
			host = rb.getString("database."+type+".host");
		driver = rb.getString("database."+type+".driver");
		url = rb.getString("database."+type+".url");
		
	}
	
	public static void main(String[] args) throws Exception {
		parsero(args);
		fill();
		Data d1 = new Data(user, pass, url+host+"/"+database1, driver, type);
		Database a = new Database(database1);
		d1.infoTablas(a);
		d1.close();
		Data d2 = new Data(user, pass, url+host+"/"+database2, driver, type);
		Database b = new Database(database2);
		d2.infoTablas(b);
		d2.close();
		a.evaluar(b);
		a.informe(b);

	}
}
