package asdd;

import java.sql.*;
import java.util.List;
import java.util.Set;

import asdd.models.*;

public class Data {
	DatabaseMetaData metaData;
	Connection connection;
	String databaseEngine;
	
	public Data(String user, String pass, String url, String driver, String databaseType) {
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, user, pass);
			metaData = connection.getMetaData();
			databaseEngine = databaseType;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void infoTablas(Database d) throws ClassNotFoundException, SQLException {
		Table t = null;
		ResultSet tables = metaData.getTables(connection.getCatalog(), null, null, new String[] { "TABLE" });
		while (tables.next()) {
			t = new Table();
			t.setName(tables.getString("TABLE_NAME")); // 3=nombre
			infoColumnas(t);
			d.addTable(t);
			t.setType(tables.getString(4)); // 4=tipo
			infoIndex(t);
			infoKeys(t);
		}
		//infoTrigger(d);
		infoProcedimientos(d.getProcedures());
		
	}

	public void infoColumnas(Table table) throws SQLException {
		ResultSet columns = metaData.getColumns(connection.getCatalog(), null, table.getName(), null);
		Column col = null;
		while (columns.next()) {
			col = new Column();
			String columnName = columns.getString("COLUMN_NAME");
			col.setName(columnName);
			col.setType(columns.getString("TYPE_NAME"));
			table.addColumn(col);
		}
	}

	public void infoKeys(Table table) throws SQLException {
		ResultSet PK = metaData.getPrimaryKeys(connection.getCatalog(), null, table.getName());
		ResultSet FK = metaData.getImportedKeys(connection.getCatalog(), null, table.getName());
		Key k = null;
		while (PK.next()) {
			k = new Key();
			k.setKeyType(1);
			k.setColumnAssoc(PK.getString("COLUMN_NAME"));
			k.setKeyName(PK.getString("PK_NAME"));
			k.setSeqNumber(PK.getInt("KEY_SEQ"));
			table.addKey(k);
		}
		while (FK.next()) {
			k = new Key();
			k.setKeyType(2);
			k.setColumnAssoc(FK.getString("FKCOLUMN_NAME"));
			k.setKeyName(FK.getString("FK_NAME"));
			// de que tabla y columna es importada
			k.referenceTo(FK.getString("PKCOLUMN_NAME"), FK.getString("PKTABLE_NAME"));
			table.addKey(k);
		}
		//no falta el unique?
	}

	public void infoProcedimientos(Set<Procedure> p) throws SQLException {
		ResultSet tr = metaData.getProcedures(connection.getCatalog(), null, null);
		Procedure aux;
		while (tr.next()) {
			aux = new Procedure(tr.getString("PROCEDURE_NAME"),tr.getString("PROCEDURE_TYPE"));
			ResultSet pp = metaData.getProcedureColumns(connection.getCatalog(), connection.getSchema(), aux.getName(), null);
			List<String[]> l = aux.getParameters();
			while (pp.next()) {
				l.add(new String[]{pp.getString("COLUMN_NAME"),pp.getString("COLUMN_TYPE")});
			}
			p.add(aux);
		}
	}

	
	public void infoTrigger(Database d) throws SQLException {
		Statement st = connection.createStatement();
		System.out.println(d.getName());
		ResultSet rs = st.executeQuery("select trigger_name, event_manipulation, event_object_table, action_timing  from information_schema.triggers where trigger_schema = \""+d.getName()+"\";");
		
		Table table;
		while (rs.next()) {
			table = d.getTable(rs.getString("EVENT_OBJECT_TABLE"));
			table.addTrigger(new Trigger(rs.getString("trigger_name"), rs.getString("event_manipulation"), rs.getString("action_timing")));
		}
	}
	
	public void infoIndex(Table table) throws SQLException {
		ResultSet ix = metaData.getIndexInfo(connection.getCatalog(), null, table.getName(), false, false);
		while (ix.next()) {
			if (!ix.getString("COLUMN_NAME").equals("null"))
				table.addIndex(ix.getString("COLUMN_NAME"));
		}
	}

	public void close() throws SQLException {
		connection.close();
	}
}
