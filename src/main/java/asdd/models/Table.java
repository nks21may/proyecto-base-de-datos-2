package asdd.models;

import java.util.Set;

import asdd.utils.Status;

import java.util.HashSet;
import java.util.Optional;

public class Table {

	private Set<Column> columns;
	private Set<Trigger> triggers;
	private Set<Key> keys;
	private String name;
	private String type;
	private Set<String> index;
	public Status status;

	public Table() {
		columns = new HashSet<Column>();
		triggers = new HashSet<Trigger>();
		keys = new HashSet<Key>();
		index = new HashSet<String>();
	}

	public Set<Column> getColumns() {
		return columns;
	}

	public Column getColumn(String name) {
		return columns.stream().filter((x) -> x.getName().equals(name)).findFirst().get();
	}

	public Trigger getTrigger(String name) {
		return triggers.stream().filter((x) -> x.getName().equals(name)).findFirst().get();
	}

	public Set<Trigger> getTriggers() {
		return triggers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Key> getKeys() {
		return keys;
	}

	public boolean addColumn(Column col) {
		return columns.add(col);
	}

	public boolean addKey(Key k) {
		return keys.add(k);
	}

	public void setType(String tableName) {
		type = tableName;
	}

	public void addIndex(String indexName) {
		index.add(indexName);
	}

	public Set<String> getIndex() {
		return this.index;
	}

	public void evaluar(Table t) {
		status = Status.EQUAL;
		t.status = Status.EQUAL;
		Set<Column> setCol = t.getColumns();
		if (!setCol.equals(columns)) {
			status = Status.DIFERENT;
			t.status = Status.DIFERENT;
		}
		Set<Column> colUniq = new HashSet<Column>();
		colUniq.addAll(columns);
		colUniq.addAll(setCol);
		colUniq.removeIf(x -> setCol.contains(x) && columns.contains(x));
		colUniq.forEach(x -> x.status = Status.UNIQUE);
		Set<Column> sameName = new HashSet<Column>();
		sameName.addAll(columns);
		sameName.removeAll(colUniq);
		sameName.forEach(x -> t.getColumn(x.getName()).evaluar(x));

		if (sameName.stream().anyMatch((x) -> x.status != Status.EQUAL) || !index.equals(t.getIndex())) {
			status = Status.DIFERENT;
			t.status = Status.DIFERENT;
		}

		keys.stream().forEach(x -> x.evaluar(t.getKey(x)));
		if (keys.stream().anyMatch((x) -> x.getStatus() != Status.EQUAL)
				|| t.keys.stream().anyMatch((x) -> x.getStatus() != Status.EQUAL)) {
			status = Status.DIFERENT;
			t.status = Status.DIFERENT;
			System.out.println(keys.stream().filter((x) -> x.getStatus() != Status.EQUAL).findAny().get());
		}
		triggers.stream().forEach(x -> x.evaluar(t.getTrigger(x.getName())));
		if (triggers.stream().anyMatch((x) -> x.getStatus() != Status.EQUAL)
				|| t.triggers.stream().anyMatch((x) -> x.getStatus() != Status.EQUAL)) {
			status = Status.DIFERENT;
			t.status = Status.DIFERENT;
		}
		
	}

	public Key getKey(Key key) {
		Optional<Key> o = keys.stream().filter(x -> x.equals(key)).findAny();
		return o.isPresent()? o.get(): null;
	}

	public void informe(Table t) {
		Set<Column> aux = new HashSet<Column>();
		switch (status) {
		case EQUAL:
			System.out.println("│ │ =" + name);
			break;
		case UNIQUE:
			System.out.print(name + ":" + type + "\n");
			break;
		case DIFERENT:
			System.out.println("│ │ " + name);

			aux.addAll(columns);
			aux.removeIf((x) -> x.status != Status.EQUAL);
			if (!aux.isEmpty()) {
				System.out.println("│ │ │ Columnas iguales");
				for (Column column : aux) {
					column.informe();
				}
			}

			aux.clear();
			aux.addAll(t.getColumns());
			aux.addAll(columns);
			aux.removeIf(x -> x.status != Status.UNIQUE);
			if (!aux.isEmpty()) {
				System.out.println("│ │ │ Columnas unicas (- para la base 1, + para la 2)");
				aux.clear();
				aux.addAll(t.getColumns());
				aux.removeIf(x -> x.status != Status.UNIQUE);
				for (Column column : aux) {
					System.out.print("│ │ │ │ +");
					column.informe();
				}
				aux.clear();
				aux.addAll(columns);
				aux.removeIf(x -> x.status != Status.UNIQUE);
				for (Column column : aux) {
					System.out.print("│ │ │ │ -");
					column.informe();
				}
			}

			aux.clear();
			aux.addAll(columns);
			aux.removeIf((x) -> x.status != Status.DIFERENT);
			if (!aux.isEmpty()) {
				System.out.println("│ │ │ Diferencia entre columnas de mismo nombre");
				for (Column column : aux) {
					column.informe();
				}
			}
			diffIndex(t);
			informarTrigger(t);
			informKey(t);
			break;
		}
	}

	private void informarTrigger(Table t) {
		Set<Trigger> aux = new HashSet<Trigger>();

		System.out.println("│ │ │ Triggers:");
		aux.addAll(triggers);
		aux.removeIf((x) -> x.getStatus() != Status.EQUAL);
		if (!aux.isEmpty()) {
			System.out.println("│ │ │ │ Triggers iguales");
			for (Trigger tr : aux)
				tr.informe();
		}

		aux.clear();
		aux.addAll(t.getTriggers());
		aux.addAll(triggers);
		aux.removeIf(x -> x.getStatus() != Status.UNIQUE);
		if (!aux.isEmpty()) {
			System.out.println("│ │ │ │ Triggers unicos (- para la base 1, + para la 2)");
			aux.clear();
			aux.addAll(t.getTriggers());
			aux.removeIf(x -> x.getStatus() != Status.UNIQUE);
			for (Trigger tr : aux) {
				System.out.print("│ │ │ │ │ +");
				tr.informe();
			}
			aux.clear();
			aux.addAll(triggers);
			aux.removeIf(x -> x.getStatus() != Status.UNIQUE);
			for (Trigger tr : aux) {
				System.out.print("│ │ │ │ │ -");
				tr.informe();
			}
		}

		aux.clear();
		aux.addAll(triggers);
		aux.removeIf((x) -> x.getStatus() != Status.DIFERENT);
		if (!aux.isEmpty()) {
			System.out.println("│ │ │ │ Diferencia entre columnas de mismo nombre");
			for (Trigger tr : aux)
				tr.informe();
		}
	}

	private void diffIndex(Table t) {
		HashSet<String> aux = new HashSet<String>();
		if (!index.equals(t.getIndex())) {
			aux.addAll(index);
			aux.addAll(t.getIndex());
			aux.removeIf((x) -> !index.contains(x) || !t.getIndex().contains(x));
			if (!aux.isEmpty()) {
				System.out.println("│ │ Indices iguales");
				for (String s : aux)
					System.out.println("    =" + s);
			}

			aux.clear();
			aux.addAll(index);
			aux.addAll(t.getIndex());
			aux.removeIf(x -> t.getIndex().contains(x) ^ index.contains(x));
			if (!aux.isEmpty()) {
				System.out.println("│ │ Indices unicos (- para la base 1, + para la 2)");
				aux.clear();
				aux.addAll(t.getIndex());
				aux.removeIf(x -> index.contains(x));
				for (String s : aux)
					System.out.println("│ │ │ +" + s);

				aux.clear();
				aux.addAll(index);
				aux.removeIf(x -> t.getIndex().contains(x));
				for (String s : aux) {
					System.out.print("│ │ │ -" + s);
				}
			}
		}
	}
	
	private void informKey(Table t) {
		Set<Key> aux = new HashSet<Key>();

		System.out.println("│ │ │ Claves: ");
		aux.addAll(keys);
		aux.removeIf((x) -> x.getStatus() != Status.EQUAL);
		if (!aux.isEmpty()) {
			System.out.println("│ │ │ │ Claves iguales en ambas DB:  ");
			for (Key k : aux)
				k.inform();
		}

		aux.clear();
		aux.addAll(t.getKeys());
		aux.addAll(keys);
		aux.removeIf(x -> x.getStatus() != Status.UNIQUE);
		if (!aux.isEmpty()) {
			System.out.println("│ │ │ │ Claves unicas (- para la base 1, + para la 2)");
			aux.clear();
			aux.addAll(t.getKeys());
			aux.removeIf(x -> x.getStatus() != Status.UNIQUE);
			for (Key k : aux) {
				System.out.print("│ │ │ │ +");
				k.inform();
			}
			aux.clear();
			aux.addAll(keys);
			aux.removeIf(x -> x.getStatus() != Status.UNIQUE);
			for (Key k : aux) {
				System.out.print("│ │ │ │ │ -");
				k.inform();
			}
		}

		aux.clear();
		aux.addAll(keys);
		aux.removeIf((x) -> x.getStatus() != Status.DIFERENT);
		if (!aux.isEmpty()) {
			System.out.println("│ │ │ │ Diferencia entre claves de mismo nombre");
			for (Key k : aux) {
				k.inform();
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Table))
			return false;
		Table other = (Table) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String r = "  Table: " + name+ "["+status+"]";
		for (Column column : columns) {
			r = r + "\n    " + column.toString();
		}
		for (Key key : keys) {
			r = r + "\n     k:"+key;
		}
		return r;
	}

	public void addTrigger(Trigger trigger) {
		triggers.add(trigger);
	}
}
