package asdd.models;

import asdd.utils.Status;

public class Column {
	
	private String name;
	private String type;
	public Status status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Column))
			return false;
		Column other = (Column) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void evaluar(Column c) {
		status = Status.EQUAL;
		c.status = Status.EQUAL;
		
		if (!type.equals(c.type)) {
			status = Status.DIFERENT;
			c.status = Status.DIFERENT;
		}
	}
	
	public void informe() {
		switch (status) {
			case EQUAL:
				System.out.println("│ │ │ │ ="+name+":"+type);
				break;
			default:
				System.out.print(name+":"+type);
				break;
		}
	}
	
	@Override
	public String toString() {
		return name+" : "+type+" ["+status+"]";
	}
}
