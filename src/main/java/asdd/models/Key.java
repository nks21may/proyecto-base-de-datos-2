package asdd.models;

import asdd.utils.Status;

public class Key {

	private String name;
	private String column;// igual que con tabla ver kionda
	private int seqNumber;
	private boolean isPrimary;
	private boolean isForeink;
	private boolean isUnique;
	private String referenceTable;
	private String referenceKey;
	private Status status;

	public Key() {
		seqNumber = -1;
		status = Status.UNIQUE;
	}

	/*
	 * 1= is primary 2= is foreink 3= is unique
	 */
	public void setKeyType(int type) {
		switch (type) {
		case 1:
			isPrimary = true;
			break;
		case 2:
			isForeink = true;
			break;
		case 3:
			isUnique = true;
			break;
		}
	}

	public void setKeyName(String keyName) {
		name = keyName;
	}

	public String getKeyName() {
		return name;
	}

	public void setColumnAssoc(String columnName) {
		column = columnName;
	}

	public void setSeqNumber(int numberOfKey) {
		seqNumber = numberOfKey;
	}

	// Cuando una clave es foranea, hay que decir a que clave hace referencia
	public void referenceTo(String refKey, String refTable) {
		this.referenceKey = refKey;
		this.referenceTable = refTable;
	}

	public int getType() {
		if (this.isPrimary)
			return 1;
		if (this.isForeink)
			return 2;
		if (this.isUnique)
			return 3;
		return 0;
	}

	public void evaluar(Key k) {
		if (k == null)
			return;
		status = Status.EQUAL;
		k.status = Status.EQUAL;
		if (isPrimary ^ k.isPrimary) {
			status = Status.DIFERENT;
			k.status = Status.DIFERENT;
		}
		if (isForeink ^ k.isForeink) {
			status = Status.DIFERENT;
			k.status = Status.DIFERENT;
		}
		if (isUnique ^ k.isUnique) {
			status = Status.DIFERENT;
			k.status = Status.DIFERENT;
		}
			
		if (!name.equals(k.name)) {
			status = Status.DIFERENT;
			k.status = Status.DIFERENT;
		}
		if (!column.equals(k.column)) {
			status = Status.DIFERENT;
			k.status = Status.DIFERENT;
			System.out.println("=============="+k);
			System.out.println("=============="+this);
		}
		if (isForeink && k.isForeink) {
			if (!referenceTable.equals(k.referenceTable)) {
				status = Status.DIFERENT;
				k.status = Status.DIFERENT;
			}
			if (!referenceKey.equals(k.referenceKey)) {
				status = Status.DIFERENT;
				k.status = Status.DIFERENT;
			}
		}
	}

	public void inform() {
		String keyType = null;
		if (this.isForeink)
			keyType = "FOREING KEY";
		if (this.isPrimary)
			keyType = "PRIMARY KEY";
		if (this.isUnique)
			keyType = "UNIQUE KEY";
		switch (status) {
		case EQUAL:
			System.out.println("│ │ │ │ │ Clave: " + this.column + "  Tipo: " + keyType);
			break;
		case DIFERENT:
			System.out.println(
					"     Nombre columna : " + this.column + "Nombre clave : " + this.name + "tipo clave : " + keyType);
			if (this.isForeink)
				System.out.println("     Tabla desde donde se importa : " + referenceTable
						+ "     Columna de donde se importa : " + referenceKey);
			break;
		default:
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((column == null) ? 0 : column.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Key))
			return false;
		Key other = (Key) obj;
		if (column == null) {
			if (other.column != null)
				return false;
		} else if (!column.equals(other.column))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Status getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "Key [name=" + name + ", column=" + column + ", isPrimary=" + isPrimary
				+ ", isForeink=" + isForeink + ", isUnique=" + isUnique + ", referenceTable=" + referenceTable
				+ ", referenceKey=" + referenceKey + ", status=" + status + "]";
	}

}
