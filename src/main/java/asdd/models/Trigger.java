package asdd.models;

import asdd.utils.Status;

public class Trigger {

	private String name;
	private String shot;
	private String action;
	private Status status;

	public Trigger(String name, String shot, String action) {
		this.name = name;
		this.shot = shot;
		this.action = action;
		status = Status.UNIQUE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShot() {
		return shot;
	}

	public void setShot(String shot) {
		this.shot = shot;
	}

	public void evaluar(Trigger t) {
		status = Status.EQUAL;
		t.status = Status.EQUAL;
		if (!name.equals(t.getName())) {
			status = Status.DIFERENT;
			t.status = Status.DIFERENT;
			return;
		}
		if (!shot.equals(t.getShot())) {
			status = Status.DIFERENT;
			t.status = Status.DIFERENT;
			return;
		}
		if (!action.equals(t.getAction())) {
			status = Status.DIFERENT;
			t.status = Status.DIFERENT;
			return;
		}
	}

	public String getAction() {
		return action;
	}

	public void informe() {
		switch (status) {
		case EQUAL:
			System.out.println("│ │ │ │ =" + name + ":" +  action + " " + shot);
			break;
		default:
			System.out.println(name + ":" + action+" "+shot);
			break;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trigger other = (Trigger) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Status getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "Trigger [name=" + name + ", shot=" + shot + ", status=" + status + "]";
	}
	
}
