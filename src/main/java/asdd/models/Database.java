package asdd.models;

import java.util.Set;

import asdd.utils.Status;

import java.util.HashSet;
import java.util.Optional;

public class Database {

	private Set<Table> tables;
	private Set<Procedure> procedures;
	private String name;
	private Status status;

	public Database(String name) {
		this.name = name;
		tables = new HashSet<Table>();
		procedures = new HashSet<Procedure>();
	}

	public Set<Table> getTables() {
		return tables;
	}

	public String getName() {
		return name;
	}

	public Table getTable(String name) {
		return tables.stream().filter((x) -> x.getName().equals(name)).findFirst().get();
	}

	public Procedure getProcedure(String name) {
		Optional<Procedure> op = procedures.stream().filter((x) -> x.getName().equals(name)).findFirst();
		return op.isPresent() ? op.get() : null;
	}

	public boolean addTable(Table table) {
		return tables.add(table);
	}

	public Set<Procedure> getProcedures() {
		return procedures;
	}

	public boolean addProcedure(Procedure proc) {
		return procedures.add(proc);
	}

	public void informe(Database d) {
		Set<Table> aux = new HashSet<Table>();
		switch (status) {
		case EQUAL:
			System.out.println("Las bases de datos son completamente iguales");
			break;
		case UNIQUE:
			System.out.println("Las bases de datos son completamente distintas");
			break;
		case DIFERENT:
			System.out.println("Las bases tiene diferencias");
			aux.addAll(d.getTables());
			aux.removeIf(x -> x.status != Status.EQUAL);
			if (!aux.isEmpty()) {
				System.out.println("│ Tablas iguales");
				for (Table t : aux)
					t.informe(null);
			}
			aux.clear();
			aux.addAll(tables);
			aux.removeIf(x -> x.status != Status.UNIQUE);
			if (!aux.isEmpty() || d.getTables().stream().anyMatch(x->x.status == Status.UNIQUE)) {
				System.out.println("│ Tablas adicionales de las bases (- para la primera, +para la segunda)");
				for (Table t : aux) {
					System.out.print("│ │ -");
					t.informe(null);
				}
				aux.clear();
				aux.addAll(d.getTables());
				aux.removeIf(x -> x.status != Status.UNIQUE);
				for (Table t : aux) {
					System.out.print("│ │ +");
					t.informe(null);
				}
			}

			aux.clear();
			aux.addAll(d.getTables());
			aux.removeIf((x) -> x.status != Status.DIFERENT);
			System.out.println("│ Diferencias entre tablas");
			for (Table t : aux) {
				getTable(t.getName()).informe(t);
				;
			}
			informProc(d);
			break;
		}

	}

	public void evaluar(Database d) {
		status = Status.EQUAL;
		d.status = Status.EQUAL;
		Set<Table> allDiferent = new HashSet<Table>();
		allDiferent.addAll(getTables());
		allDiferent.addAll(d.getTables());
		allDiferent.removeIf(x -> d.getTables().contains(x) && tables.contains(x));
		allDiferent.forEach(x -> x.status = Status.UNIQUE);
		if (!allDiferent.isEmpty())
			status = Status.UNIQUE;

		Set<Table> sameNameA = new HashSet<Table>();
		sameNameA.addAll(getTables());
		sameNameA.removeAll(allDiferent);
		sameNameA.forEach(x -> d.getTable(x.getName()).evaluar(x));
		sameNameA.removeIf(x -> x.status != Status.DIFERENT);
		if (!sameNameA.isEmpty()) {
			status = Status.DIFERENT;
			d.status = Status.DIFERENT;
		}
		procedures.forEach(x -> x.evaluar(d.getProcedure(x.getName())));
	}

	private void informProc(Database d) {
		Set<Procedure> aux = new HashSet<Procedure>();
		switch (status) {
		case EQUAL:
			System.out.println("│ Los procedimientos son iguales");
			break;
		default:
			System.out.println("│ Procedimientos:");
			aux.addAll(d.getProcedures());
			aux.removeIf(x -> x.status != Status.EQUAL);
			if (!aux.isEmpty()) {
				System.out.println("│ │ Procedimientos iguales");
				for (Procedure p : aux)
					p.informe();
			}
			aux.clear();
			aux.addAll(getProcedures());
			aux.removeIf(x -> x.status != Status.UNIQUE);
			if (aux.isEmpty() || d.getProcedures().stream().anyMatch(x -> x.status == Status.UNIQUE)) {
				System.out.println("│ │ Procedimientos adicionales (- para la primera, +para la segunda)");
				for (Procedure p : aux) {
					System.out.print("│ │ │ -");
					p.informe();
				}
				aux.clear();
				aux.addAll(d.getProcedures());
				aux.removeIf(x -> x.status != Status.UNIQUE);
				for (Procedure p : aux) {
					System.out.print("\n│ │ │ +");
					p.informe();
				}
			}

			aux.clear();
			aux.addAll(d.getProcedures());
			aux.removeIf((x) -> x.status != Status.DIFERENT);
			System.out.println("│ │ Diferencias entre procedimientos");
			for (Procedure p : aux) {
				p.informe();
			}
			break;
		}

	}

	@Override
	public String toString() {
		String r = "Database: " + name + " [" + status + "]";
		for (Table table : tables) {
			r = r + "\n" + table.toString();
		}
		return r;
	}
}
