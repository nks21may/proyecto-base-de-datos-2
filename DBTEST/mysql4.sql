delimiter $$ 
CREATE DEFINER = 'vendedor3' PROCEDURE noValido()
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE a INT;
	DECLARE curso CURSOR FOR select f.nro_factura from Factura f where (select (cantidad * precio) as n from ItemFactura where nro_factura = f.nro_factura) <> f.monto;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'
		BEGIN
			SET done = 1;
		END;
	OPEN curso;
	REPEAT
		FETCH curso INTO a;
		IF NOT done THEN
			INSERT INTO dat1.nocons(nro_factura) VALUE (a);
		END IF;
	UNTIL done END REPEAT;
	CLOSE curso;
END$$

delimiter ;
