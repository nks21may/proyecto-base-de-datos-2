CREATE DATABASE IF NOT EXISTS dat1;

#Cliente (nro_cliente, apellido, nombre, dirección, teléfono)
CREATE TABLE IF NOT EXISTS dat1.Cliente(
    nro_cliente INT NOT NULL AUTO_INCREMENT,
    apellido VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    telefono INT NOT NULL,
    PRIMARY KEY (nro_cliente)
)ENGINE = InnoDB;

#Producto (cod_producto, descripción, precio, stock_minimo, stock_maximo, cantidad)
CREATE TABLE IF NOT EXISTS dat1.Producto(
  cod_producto INT NOT NULL AUTO_INCREMENT,
  descripción VARCHAR(255),
  stock_minimo INT NOT NULL CHECK(stock_minimo >0),
  stock_maximo INT NOT NULL CHECK(stock_maximo >0),
  PRIMARY KEY (cod_producto)
) ENGINE = InnoDB;

#Factura (nro_factura, nro_cliente, fecha, monto)
CREATE TABLE IF NOT EXISTS dat1.Factura (
  nro_factura INT AUTO_INCREMENT NOT NULL,
  nro_cliente INT NOT NULL,
  fecha DATE,
  monto INT NOT NULL,
  max INT NOT NULL,
  FOREIGN KEY (nro_cliente) REFERENCES Cliente(nro_cliente),
  PRIMARY KEY (nro_factura)
) ENGINE = InnoDB;

#ItemFactura (cod_producto, nro_factura, cantidad, precio)
CREATE TABLE IF NOT EXISTS dat1.ItemFactura(
  cod_producto INT NOT NULL,
  nro_factura INT NOT NULL,
  cantidad INT NOT NULL CHECK(cantidad > 0),
  precio INT NOT NULL CHECK(precio > 0),
  PRIMARY KEY (nro_factura, cod_producto),
  FOREIGN KEY (cod_producto) REFERENCES Producto(cod_producto),
  FOREIGN KEY (nro_factura) REFERENCES Factura(nro_factura)
) ENGINE = InnoDB;
