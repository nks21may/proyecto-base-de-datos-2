CREATE DATABASE IF NOT EXISTS dat2;

#Cliente (nro_cliente, apellido, nombre, dirección, teléfono)
CREATE TABLE IF NOT EXISTS dat2.Cliente(
    nro_cliente INT NOT NULL AUTO_INCREMENT,
    apellido VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    PRIMARY KEY (nro_cliente)
)ENGINE = InnoDB;

#Producto (cod_producto, descripción, precio, stock_minimo, stock_maximo, cantidad)
CREATE TABLE IF NOT EXISTS dat2.Producto(
  cod_producto VARCHAR(100) NOT NULL AUTO_INCREMENT,
  descripción VARCHAR(255),
  paraProbarLasForaneas INT,
  stock_minimo INT NOT NULL CHECK(stock_minimo >0),
  stock_maximo INT NOT NULL CHECK(stock_maximo >0),
  FOREIGN KEY (paraProbarLasForaneas) REFERENCES Cliente(nro_cliente),
  PRIMARY KEY (cod_producto)
) ENGINE = InnoDB;

#Factura (nro_factura, nro_cliente, fecha, monto)
CREATE TABLE IF NOT EXISTS dat2.Factura (
  nro_factura INT AUTO_INCREMENT NOT NULL,
  nro_cliente INT NOT NULL,
  stock_maximo INT NOT NULL CHECK(stock_maximo >0),
  fecha DATE,
  monto INT NOT NULL,
  max INT NOT NULL,
  FOREIGN KEY (max) REFERENCES Producto(cod_producto),
  FOREIGN KEY (nro_cliente) REFERENCES Cliente(nro_cliente),
  PRIMARY KEY (nro_factura)
) ENGINE = InnoDB;
