delimiter $$
DROP TRIGGER IF EXISTS checkProducto; $$
CREATE TRIGGER checkProducto BEFORE INSERT ON dat1.Producto FOR EACH ROW
BEGIN
    IF  (0 > NEW.stock_minimo) OR (0 > NEW.stock_maximo) THEN
    	signal sqlstate '45000';
    END IF;
END; $$

DROP TRIGGER IF EXISTS checkItemFactura; $$
CREATE TRIGGER checkItemFactura BEFORE INSERT ON dat1.ItemFactura FOR EACH ROW
BEGIN
    IF  (0 > NEW.cantidad OR 0 > NEW.precio) THEN
    	signal sqlstate '45000';
    END IF;
END;$$

delimiter ;
